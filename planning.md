* [x] Fork and clone the starter project from django-one-shot 
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
  * [x] python -m venv .venv 
* [x] Upgrade pip
  * [x] python -m pip install --upgrade pip
* [x] Install django
  * [x] pip install django  
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8
* [x] Install djlint
  * [x] pip install djlint
  * [x] install debug toolbar
    * [x] pip install django-debug-toolbar  
* [x] Deactivate your virtual environment
  * [x] deactivate 
* [x] Activate your virtual environment
  * [x] source .venv/bin/activate
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt 
  * [x] create django project brain_two
    * [x] django-admin startproject brain_two .
  * [x] create app todos
    * [x] python manage.py startapp todos
* [x] add app into settings.py installed app
  * [x] "todos.apps.TodosConfig"
* [x] run migrations
  * [x] python manage.py makemigrations
  * [x] python manage.py migrate
* [x] create super user 
  * [x] python manage.py createsuperuser
  * [x] create TodoList model in models.py
    * [x] containing name and created_on 
    * [x] __str__(self) mehtod for name 
* [x] register TodoList model with admin
  * [x] from todos.models import TodoList 
  * [x] admin.site.register(TodoList)
* [x] make TodoItem models in models.py
    * [x] include task,due_date,is_completed, list 
    * [x] register TodoItem model with admin 
* [x] Create view to get all instances of the TodoList model 
* [x] register view in todos app for the path "" 
* [x] include URL patterns from todos app in brain_two project 
* [x] Create template for the list view 
  * [x] create template for list,create,update,delete 
  * [x] import in url
  * [x] create form for create view
  * [x] create a update view
  * [x] create delete view 
  * [x] create create view for the todoItem model to show task field in the form and handle the form submission to create a new todoitem 
  * [ ] create update view for todoitem model


## Resources
<https://learn-2.galvanize.com/cohorts/3352/blocks/1859/content_files/build/02-django-one-shot/65-django-one-shot-00.md>

<https://docs.djangoproject.com/en/4.0/ref/models/fields/>

